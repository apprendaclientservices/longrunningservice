using System.Runtime.Serialization;

namespace Contracts
{
    [DataContract(Namespace = "Service", Name = "DataTransfer")]
    public class DataTransferObject
    {

        [DataMember]
        public int Result { get; set; }
    }
}
