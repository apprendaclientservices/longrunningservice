# README #

This application demonstrates how to create a WCF service for the Apprenda platform which will initialize some objects before any of the contract endpoints are touched. Examples of what might need to get intialized in this way could be an IoC container or an event listener of some sort. 

In fact, this service does not have to have any endpoints defined at all.

To make it 'long running', use the Apprenda min instance count mechanism. This platform feature in conjunction with a static initializer is all that is needed for a long running service.

In addition to the static initializer, we've included a method (called from the initializer) which generates a hearbeat in the form of debug logging. The heartbeat is not necessary to have a long running service but does demonstrate the utility of this pattern.

### Who do I talk to? ###

* @mak3r